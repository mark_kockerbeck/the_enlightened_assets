#!/bin/bash

PWD=$(pwd)
cd effects
for f in *.wav; do
    name=`echo $f | cut -d'.' -f1`;
    echo $name
    ffmpeg -y -i "$f" -vn -ar 44100 -ac 2 -ab 192k -f mp3 "../audio/${name}.mp3"
done
cd $PWD