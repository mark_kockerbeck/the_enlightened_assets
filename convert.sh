#!/bin/bash

PWD=$(pwd)
cd audio
for f in *.mp3; do
    ffmpeg -i $f -ac 2 -codec:a libmp3lame -b:a 48k -ar 16000 $f-new.mp3
    rm $f
    mv $f-new.mp3 $f
done
cd $PWD
