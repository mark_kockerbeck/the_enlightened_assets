#!/bin/bash

for i in $(seq 1 3); do
    rm audio/$i.mp3
    ls -1 audio/$i-*.mp3 2> /dev/null | while read fn ; do cat "$fn" >> audio/$i.mp3; done
done
