#!/usr/bin/env python
import sys
from os import listdir
from os.path import isfile, join
from boto3 import Session

ignore_list = [".py", ".mp3", ".sh"]
target = sys.argv[1] if len(sys.argv) > 1 else None

def should_include(f):
    if not isfile(f):
        return False
    
    for i in ignore_list:
        if f.endswith(i):
            return False
    
    if target and target not in f:
        return False
    
    return True

files = [f for f in listdir("ssml") if should_include(join("ssml", f))]
session = Session()
polly = session.client("polly")
print(files)

for f in files:
    data = open(join("ssml", f), 'r').read()
    voice = f[f.index(".")+1:]
    new_f = f[:f.index(".")] + ".mp3"
    print("Synthesizing %s with %s" % (f, voice))
    response = polly.synthesize_speech(Text = data, TextType="ssml",  VoiceId=voice, OutputFormat="mp3")
    with open(join("./audio", new_f), 'wb') as fw:
        fw.write(response["AudioStream"].read())
    print("Saved %s" % new_f)
