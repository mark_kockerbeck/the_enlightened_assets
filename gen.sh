#!/bin/bash
if [ $# -eq 0 ]
    then
    echo "Generating EVERYTHING"
    rm -rf audio/*.mp3 \
        && ./synth.py \
        && ./speedup.sh \
        && ./effects.sh \
        && ./concat.sh \
        && ./convert.sh 
else
    echo "Generating $1"
    rm -f audio/$1.mp3 \
        && ./synth.py $1 \
        && ./speedup.sh $1 \
        && ./effects.sh $1 \
        && ./concat.sh $1 \
        && ./convert.sh $1    
fi