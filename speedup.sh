#!/bin/bash

if [ $# -eq 0 ]
then
    for f in audio/*.mp3; do
        name=`echo $f | cut -d'.' -f1`;
        ffmpeg -y -i "$f" -filter:a "atempo=1.3" -vcodec copy -af "volume=10dB" "${name}-new.mp3"
        rm "$f"
        mv "${name}-new.mp3" $f
    done
else
    f=audio/${1/./-}.mp3
    name=`echo $f | cut -d'.' -f1`;
    ffmpeg -y -i "$f" -filter:a "atempo=1.3" -vcodec copy -af "volume=10dB" "${name}-new.mp3"
    rm "$f"
    mv "${name}-new.mp3" $f
fi
